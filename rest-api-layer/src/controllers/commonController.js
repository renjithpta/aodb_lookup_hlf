

/****
 *@Description : Controller to handle v.
 *@author Renjith .
 *@license Apache 2.0 
 */


/*
* API - @GET /health . Consume and Produce JSON payload
* @Description  : Handles helath check

* @response  { status : string  , success : boolean} : JSON payload
*/
exports.helath = async (req, res) => {
  

    return res.json({"status" :"up"});

};

/*
* API - @GET /cell . Consume and Produce JSON payload
* @Description  : Handles helath check

* @response  { status : string  , success : boolean} : JSON payload
*/
exports.cell = async (req, res) => {
  

    return res.json({"status" :"up"});

};



/*
* API - @POST /v1/api/lookupandduration/callsignfromflight . Consume and Produce JSON payload
* @Description  : Handles lookupcall to find the callsign from the flight details such scheduledFlightNo , date and A/D indicator.
* @Params { scheduleFlight : string, scheduleddate : string , arrivalOrDept : string } : JSON payload
* @response  { callsign : string } : JSON payload
*/
exports.callSignFromFlight = async (req, res) => {
    console.log(`======start ====callSignFromFlight==========================`.yellow.bold)
    const { scheduleFlight, scheduleDate, arrivalOrDept } = req.body;
    console.log("Arival dept",arrivalOrDept )
    if(arrivalOrDept !== 'A' && arrivalOrDept !== 'D')   return res.status(400).json({ "error": "Wrong input arrival or depature" });
    if(scheduleDate.length !== 8 || isNaN(scheduleDate)) return res.status(400).json({ "error": "Wrong input date" });

    let networkObj  = await network.connectToNetwork(req.username.trim());
    if (networkObj.err) {
        res.send(networkObj);
    }
    network.query(networkObj, "callSignFromFlight", scheduleFlight, scheduleDate, arrivalOrDept)
        .then(result => {
            console.log(`=========Received=======${result}====`.bgGreen.bold);
            if (result) {
                if (result.length > 0) {
                   
                    return res.status(200).json({ "callSign": result.toString() });
                }
            }
            return res.status(500).json({ error: 'Something went wrong' });
        })
        .catch((err) => {
            console.log(`=========Error=======${err}====`.bgRed.bold);
            return res.status(500).json({ error: `Something went wrong\n ${err}` });
        });
};

