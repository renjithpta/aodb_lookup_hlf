

/****
 *@Description : Router - API url  hadler configurations.
 *@author Renjith .
 *@license Apache 2.0 
 */
const express = require('express');
const router = express.Router();
const commonController = require('../controllers/commonController');


router.get('',  commonController.helath);
     
router.get('/cell',  commonController.cell);

module.exports = router;
